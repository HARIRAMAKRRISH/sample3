import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserDetails } from '../model/userDetails';
import { ServiceService } from '../service/service.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  firstname!: string;
  lastname!:string;
  email!:string;
  phone!:string;
  password!:string;
  confirmpassword!:string ;
  // user:UserDetails | undefined;
  // constructor(private userservice:UserService,private router:Router) {
  //  
  //user: LoginUser
  newUser: UserDetails
  constructor(private us: ServiceService,private route:Router) {
   // this.user = { 'email': '', 'password': '' };
    this.newUser = { 'firstname': '','lastname':'', 'email': '', 'phone': '','password':'','confirmpassword':''}
  }
  ngOnInit(): void {
  }

  // register()
  // {
  //   this.user=new UserDetails(this.firstname,this.lastname,this.email,this.phone,this.password,this.confirm_password);
  //   this.userservice.addUser(this.user);
  //   console.log(this.userservice);
  //   this.router.navigate(['/login']);
  // }
  register() {
   
    console.log(this.newUser)
    // console.log(this.confirm_password)
    console.log(this.newUser, 'login');
    this.us.registerUser(this.newUser)
    .subscribe(res=>{
      console.log(res);
      alert("USER Registered Successfully");
      this.route.navigate(['login']);
    },
    err=>{
      alert("Something went wrong")
    }
    )

    console.log(this.newUser)

  }
}

