export class Book{
   constructor(public name:string,
       public author:string,
       public language:string,
       public specialization:string,
       public content:string,
       public imageId:string,
       public id?:number){}     
}