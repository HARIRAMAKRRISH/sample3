export class AdminUserDetails{
   constructor(public firstname:string,
      public lastname:string,
      public email:string,
      public phone:string,
      public password:string,
      public confirmpassword:string,
      public id?:number){
   }
   
}