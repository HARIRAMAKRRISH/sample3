import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginUser } from '../model/LoginUser';
import { UserDetails } from '../model/userDetails';
import { ServiceService } from '../service/service.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email!:string;
  password!:string;
  user: LoginUser
  // newUser: User
  constructor(private us: ServiceService,private route:Router) {
    this.user = { 'email': '', 'password': '' };
    // this.newUser = { 'username': '', 'password': '', 'email': '' }
  }

  ngOnInit(): void {
  }
  // login()
  // {
  //   let error:string='error';
  //   let item=this.userservice.getUserByEmail(this.email);
  //   for(let i=0;i<=item.length;i++){
  //     if(this.email===item[i].email && this.password===item[i].password){
  //       localStorage.setItem('email',item[i].firstname);
  //       console.log(this.email);
  //       this.router.navigate(['/dashboard']);
  //       error='success';
  //       break;
  //     }
  //   }
  //   if(error==='error'){
      
  //   }
  // }
  login() {

    let redirectTo = localStorage.getItem('redirectTo');
    
    let email:string = this.user.email;
    let password:string= this.user.password;

    this.us.loginUser(email,password)
    .subscribe(res=>{
      console.log(res)
      localStorage.setItem('name',email)
      if(redirectTo){
        this.route.navigate([redirectTo]);
      }
      else{
        alert("Loggid In");
      this.route.navigate(['products']);
      }
    },
    err=>{
      alert("Invalid credentials")
    }
    )
  }

}
